import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
blinkrate = 0.5
for x in range(10):
    GPIO.output(17, True)
    time.sleep(blinkrate)
    GPIO.output(17, False)
    time.sleep(blinkrate)
    
GPIO.cleanup()